import React, { Component } from 'react';
import './App.css';
import Post from './components/Post/Post'
import Button from "./components/Button/Button";

class App extends Component {

  state = {
    posts: [],
  };

  loadData =() => {
    fetch('https://api.chucknorris.io/jokes/random').then(response => {
      if (response.ok) {
        return response.json()
      }
      throw new Error('Something wrong with request');
    }).then(posts => {
      this.setState({posts: posts.value});
    }).catch(error => {
      console.log(error);
    });
  };

  componentDidMount() {
    this.loadData();
  }

  render() {
    return (
      <div className="App">
        <Button
          btnType="Success"
          onClick={this.loadData}
        >
          SHOW JOKES
        </Button>
        <Post
          value={this.state.posts}
        />
      </div>
    );
  }
}

export default App;
